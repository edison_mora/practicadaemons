CC = gcc
CFLAGS = -O2 -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread -lm

all: fft_service fft_client

fft_service: fft_service.c csapp.o fft.o
	$(CC) $(CFLAGS) -o fft_service fft_service.c csapp.o fft.o $(LIB)

fft_client: fft_client.c csapp.o fft.o
	$(CC) $(CFLAGS) -o fft_client fft_client.c csapp.o fft.o $(LIB)

fft.o: fft.c
	$(CC) $(CFLAGS) -c fft.c

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c csapp.c

clean:
	rm -f *.o fft_service fft_client *~

